<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends PHPUnit_Framework_TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        return array(
            array(4, 2, 2),
            array(6, 3, 2)
            array(0, 7, 0),	//When $guess become zero doesn't even get into the loop
			array(0, 7, 2), //If $amount=7 (except multiplying with 1) all results are gonna be wrong
			array(0,0.7,0.5)//Also wrong if (0.49<$amount<1 and 0<multiplier<1) because ($estimatedResult - $amount) is
							//not bigger than 0.49 so that's not get into the loop
        );
    }
}
