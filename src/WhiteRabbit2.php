<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
   	$coins = array(100,50,20,10,5,2,1);	
    $coinsCount = array(0,0,0,0,0,0,0);

    $i=0;
	while($amount!=0) { //Loop Until Become 0

	if($amount/$coins[$i]>=1) { //If amount/CurrentCoin >= 1 still there is current banknote inside of amount 
              
    $amount-=$coins[$i]; //Decrease current coin from amount
    $coinsCount[$i]++;}	//Add plus 1 coinsCount
                
    else $i++;						
                            }
					
	return array(			
    '1'   => $coinsCount[6],
    '2'   => $coinsCount[5],
    '5'   => $coinsCount[4],
    '10'  => $coinsCount[3],
    '20'  => $coinsCount[2],
    '50'  => $coinsCount[1],
    '100' => $coinsCount[0]); 
    }
}