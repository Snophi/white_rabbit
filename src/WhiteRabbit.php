<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
       		$text = file_get_contents($filePath);			//Get content of txt

			$text = strtolower($text);						//Convert lowercase all of it

			$text = preg_replace('/[^a-z]/','',$text);		//Make only alphabetic characters

			return $text;									//Return text
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
            $LetterText = count_chars($parsedFile,1);		//Count each letter

			asort($LetterText);								//Sort array but keep their keys

			$keys = array_keys($LetterText);				//Save their keys

			$index = floor(((count($keys)-1)/2));			//Find the middle value or the lowest middle value (floor)

			$letter = chr($keys[$index]);					//Returns a one-character string 

			$occurrences = $LetterText[$keys[$index]];		//Shows how many of letter here

			return $letter;									//Return Median
    }
}